<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;
use App\Models\Form;
use App\Models\Field;
use App\Models\FieldValue;
use Ramsey\Uuid\Uuid;

class StartupApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $startupForm = Form::where('type', 'startup')->first();
        $contact = Field::create([
            'name' => 'Контакное лицо (ФИО)',
            'uuid' => Uuid::uuid1(),
            'type' => 'text',
            'description' => 'Контакное лицо (ФИО)',
            'status' => 'enabled',
            'required' => true,
            'order' => 1
        ]);
        $contact->forms()->attach($startupForm->id);

        $telephone = Field::create([
            'name' => 'Телефон',
            'uuid' => Uuid::uuid1(),
            'type' => 'text',
            'description' => 'Формат: 7 701 222 2525',
            'status' => 'enabled',
            'required' => true,
            'order' => 2
        ]);
        $telephone->forms()->attach($startupForm->id);
        

        $city = Field::create([
            'name' => 'Город',
            'uuid' => Uuid::uuid1(),
            'type' => 'text',
            'description' => 'Город, где вы базируйтесь',
            'status' => 'enabled',
            'required' => true,
            'order' => 3
        ]);
        $city->forms()->attach($startupForm->id);
        

        $name = Field::create([
            'name' => 'Название стартапа',
            'uuid' => Uuid::uuid1(),
            'type' => 'text',
            'description' => 'Название вашего проекта или стартапа',
            'status' => 'enabled',
            'required' => true,
            'order' => 4
        ]);
        $name->forms()->attach($startupForm->id);

        $stage = Field::create([
            'name' => 'Стадия',
            'uuid' => Uuid::uuid1(),
            'type' => 'select',
            'description' => 'Текущая стадия проекта',
            'status' => 'enabled',
            'required' => true,
            'order' => 5
        ]);
        $stage->forms()->attach($startupForm->id);
        $stageValues = ['Есть прототип (без доходов)', 'Готовый продукт (без доходов)','Готовый продукт(приносит доход)', 'Компания с прибылью'];
        $this->createFieldValues($stage, $stageValues);

        $decription = Field::create([
            'name' => 'Краткое описание',
            'uuid' => Uuid::uuid1(),
            'type' => 'textarea',
            'description' => 'Опишите суть вашего проекта одним предложением.',
            'status' => 'enabled',
            'required' => true,
            'order' => 6
        ]);
        $decription->forms()->attach($startupForm->id);
        

        $category = Field::create([
            'name' => 'Категория',
            'uuid' => Uuid::uuid1(),
            'type' => 'select',
            'description' => 'Выберите сферу деятельности вашего проекта.',
            'status' => 'enabled',
            'required' => true,
            'order' => 7
        ]);
        $category->forms()->attach($startupForm->id);
        $categoryValues = [ 'BioTech/BioMed', 'Интернет проекты','Наукоёмкие отрасли','Телеком/связь','Традиционный бизнес'];
        $this->createFieldValues($category, $categoryValues);

        $logo = Field::create([
            'name' => 'Лого',
            'uuid' => Uuid::uuid1(),
            'type' => 'file',
            'description' => 'Загрузите лого вашего проекта',
            'status' => 'enabled',
            'required' => false,
            'order' => 8
        ]);
        $logo->forms()->attach($startupForm->id);
        $categoryValues = ['image'];
        $this->createFieldValues($logo, $categoryValues);
        
        $problem = Field::create([
            'name' => 'Решаемая проблема:',
            'uuid' => Uuid::uuid1(),
            'type' => 'textarea',
            'description' => 'Назовите проблему на рынке, которую решает проект, и опишите ваш способ ее решения.',
            'status' => 'enabled',
            'required' => true,
            'order' => 9
        ]);
        $problem->forms()->attach($startupForm->id);

        $analytics = Field::create([
            'name' => 'Аналитика рынка',
            'uuid' => Uuid::uuid1(),
            'type' => 'textarea',
            'description' => 'Обоснуйте актуальность вашего проекта аналитическими данными о рынке: цифры, прогнозы, сделки.',
            'status' => 'enabled',
            'required' => true,
            'order' => 10
        ]);
        $analytics->forms()->attach($startupForm->id);

        $competition = Field::create([
            'name' => 'Основные конкуренты',
            'uuid' => Uuid::uuid1(),
            'type' => 'textarea',
            'description' => 'Перечислите основные аналоги вашего проекта как на домашнем рынке, так и на зарубежных.',
            'status' => 'enabled',
            'required' => true,
            'order' => 11
        ]);
        $competition->forms()->attach($startupForm->id);

        $pitch = Field::create([
            'name' => 'Уникальное торговое предложение',
            'uuid' => Uuid::uuid1(),
            'type' => 'textarea',
            'description' => 'Чем вы превосходите своих конкурентов?',
            'status' => 'enabled',
            'required' => true,
            'order' => 12
        ]);
        $pitch->forms()->attach($startupForm->id);
        

        $technology = Field::create([
            'name' => 'Суть применяемой технологии',
            'uuid' => Uuid::uuid1(),
            'type' => 'textarea',
            'description' => 'Опишите какая технология/технологии используются в вашем проекте',
            'status' => 'enabled',
            'required' => true,
            'order' => 13
        ]);
        $technology->forms()->attach($startupForm->id);
        

        $businessModel = Field::create([
            'name' => 'Тип бизнес-модели',
            'uuid' => Uuid::uuid1(),
            'type' => 'select',
            'description' => 'Тип бизнес-модели',
            'status' => 'enabled',
            'required' => true,
            'order' => 14
        ]);
        $businessModel->forms()->attach($startupForm->id);
        $businessModelValues = ['B2B','B2C','C2C','B2B2C','B2G'];
        $this->createFieldValues($businessModel, $businessModelValues);

        $monetization = Field::create([
            'name' => 'Каналы монетизации',
            'uuid' => Uuid::uuid1(),
            'type' => 'textarea',
            'description' => 'Укажите способы монетизации проекта.',
            'status' => 'enabled',
            'required' => true,
            'order' => 15
        ]);
        $monetization->forms()->attach($startupForm->id);
        

        $customerAcquisition = Field::create([
            'name' => 'Привлечение и удержание клиентов',
            'uuid' => Uuid::uuid1(),
            'type' => 'textarea',
            'description' => 'Привлечение и удержание клиентов:',
            'status' => 'enabled',
            'required' => false,
            'order' => 16
        ]);
        $customerAcquisition->forms()->attach($startupForm->id);
        

        $team = Field::create([
            'name' => 'Информация о команде',
            'uuid' => Uuid::uuid1(),
            'type' => 'textarea',
            'description' => 'Количество сотрудников, их опыт, организация рабочего процесса и прочая важная информация.',
            'status' => 'enabled',
            'required' => false,
            'order' => 17
        ]);
        $team->forms()->attach($startupForm->id);

        $website = Field::create([
            'name' => 'Cайт',
            'uuid' => Uuid::uuid1(),
            'type' => 'text',
            'description' => 'Вебсайт',
            'status' => 'enabled',
            'required' => false,
            'order' => 18
        ]);
        $website->forms()->attach($startupForm->id);
        

        $year = Field::create([
            'name' => 'Год основания',
            'uuid' => Uuid::uuid1(),
            'type' => 'text',
            'description' => 'Год основания',
            'status' => 'enabled',
            'required' => false,
            'order' => 19
        ]);
        $year->forms()->attach($startupForm->id);

        $presentation = Field::create([
            'name' => 'Презентация',
            'uuid' => Uuid::uuid1(),
            'type' => 'file',
            'description' => 'Загрузите вашу презентацию',
            'status' => 'enabled',
            'required' => true,
            'order' => 20
        ]);
        $presentation->forms()->attach($startupForm->id);
        

        $presentationValues = ['ppt'];
        $this->createFieldValues($presentation, $presentationValues);

        $achievements = Field::create([
            'name' => 'Текущие показатели и достижения проекта',
            'uuid' => Uuid::uuid1(),
            'type' => 'textarea',
            'description' => 'Чего проекту удалось добиться на данный момент (выручка, прибыль, уникальные посетители, платящие клиенты, повторные пользователи, количество партнеров и прочие достижения)',
            'status' => 'enabled',
            'required' => true,
            'order' => 21
        ]);
        $achievements->forms()->attach($startupForm->id);

        $round = Field::create([
            'name' => 'Раунд',
            'uuid' => Uuid::uuid1(),
            'type' => 'select',
            'description' => 'Укажите, какой раунд инвестиций вы поднимаете.',
            'status' => 'enabled',
            'required' => true,
            'order' => 22
        ]);
        $round->forms()->attach($startupForm->id);
        $roundValues = ['Посевной','Первый раунд','Второй и последующие раунды'];
        $this->createFieldValues($round, $roundValues);

        $sum = Field::create([
            'name' => 'Требуемая сумма',
            'uuid' => Uuid::uuid1(),
            'type' => 'number',
            'description' => 'Назовите общую сумму, необходимую для закрытия раунда. Сумма указывается в USD.',
            'status' => 'enabled',
            'required' => true,
            'order' => 23
        ]);
        $sum->forms()->attach($startupForm->id);

    }

    private function createFieldValues($field, $values)
    {
        foreach ($values as $value) {
            FieldValue::create([
                'field_id' => $field->id,
                'value' => $value
            ]);
        }
    }
}
