<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;
use App\Models\Form;
use App\Models\Field;
use App\Models\FieldValue;
use Ramsey\Uuid\Uuid;

class InvestorApplicationSeeder extends Seeder
{
    public function run()
    {
        $investorForm = Form::where('type', 'investor')->first();
        $contact = Field::create([
            'name' => 'Контакное лицо (ФИО)',
            'uuid' => Uuid::uuid1(),
            'type' => 'text',
            'description' => 'Контакное лицо (ФИО)',
            'status' => 'enabled',
            'required' => true,
            'order' => 1
        ]);
        $contact->forms()->attach($investorForm->id);

        $telephone = Field::create([
            'name' => 'Телефон',
            'uuid' => Uuid::uuid1(),
            'type' => 'text',
            'description' => 'Формат: 7 701 222 2525',
            'status' => 'enabled',
            'required' => true,
            'order' => 2
        ]);
        $telephone->forms()->attach($investorForm->id);

        $decription = Field::create([
            'name' => 'Краткое описание инвестиционной деятельности',
            'uuid' => Uuid::uuid1(),
            'type' => 'textarea',
            'description' => 'Опишите опыт ваших инвестиций',
            'status' => 'enabled',
            'required' => true,
            'order' => 6
        ]);
        $decription->forms()->attach($investorForm->id);

        $category = Field::create([
            'name' => 'Категория проектов для возможных инвестиций',
            'uuid' => Uuid::uuid1(),
            'type' => 'select',
            'description' => 'Категория проектов для возможных инвестиций',
            'status' => 'enabled',
            'required' => true,
            'order' => 7
        ]);
        $category->forms()->attach($investorForm->id);
        $categoryValues = [ 'BioTech/BioMed', 'Интернет проекты','Наукоёмкие отрасли','Телеком/связь','Традиционный бизнес'];
        $this->createFieldValues($category, $categoryValues);

        $team = Field::create([
            'name' => 'Кол-во проинвестированных проектов',
            'uuid' => Uuid::uuid1(),
            'type' => 'number',
            'description' => 'Кол-во проинвестированных проектов',
            'status' => 'enabled',
            'required' => false,
            'order' => 17
        ]);
        $team->forms()->attach($investorForm->id);

        $sum = Field::create([
            'name' => 'Планируемая сумма инвестирования',
            'uuid' => Uuid::uuid1(),
            'type' => 'select',
            'description' => 'Назовите общую сумму, необходимую для закрытия раунда. Сумма указывается в USD.',
            'status' => 'enabled',
            'required' => true,
            'order' => 23
        ]);
        $sum->forms()->attach($investorForm->id);
        $sumValues = [ '0-10000', '10 000-25 000','25 000-100 000','100 000+'];
        $this->createFieldValues($sum, $sumValues);
    }

    private function createFieldValues($field, $values)
    {
        foreach ($values as $value) {
            FieldValue::create([
                'field_id' => $field->id,
                'value' => $value
            ]);
        }
    }
}
