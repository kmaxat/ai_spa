<?php

use App\Models\Field;
use App\Models\Form;
use App\Models\Message;
use App\Models\Role;
use App\Models\Shortcut;
use App\Models\Submission;
use App\Models\SubmissionValue;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $startupRole = Role::create([
            'name' => 'startup',
            'display_name' => 'Startup',
            'description' => 'Startup'
        ]);

        $investorRole = Role::create([
            'name' => 'investor',
            'display_name' => 'Investor',
            'description' => 'Investor'
        ]);

        $adminRole = Role::create([
            'name' => 'admin',
            'display_name' => 'admin',
            'description' => 'Administrator'
        ]);

        $startup = User::create([
            'email' => 'startup@ai.kz',
            'password' => bcrypt('qweqwe'),
            'confirmed' => true,
        ]);
        $investor = User::create([
            'email' => 'investor@ai.kz',
            'password' => bcrypt('qweqwe'),
            'confirmed' => true,
        ]);

        $admin = User::create([
            'email' => 'kmaxat@gmail.com',
            'password' => bcrypt('qweqwe'),
            'confirmed' => true,
        ]);

        $investor->roles()->attach($investorRole->id, ['status' => 0]);
        $startup->roles()->attach($startupRole->id, ['status' => 1]);
        $admin->roles()->attach($adminRole->id, ['status' => 1]);

        $startupForms = Form::create([
            'name' => 'Startups 2017',
            'type' => 'startup',
        ]);
        $investorsForm = Form::create([
            'name' => 'Investors 2017',
            'type' => 'investor',
        ]);
        $this->call(StartupApplicationSeeder::class);
        $this->call(InvestorApplicationSeeder::class);

        $faker = Faker\Factory::create();
        factory(App\Models\Event::class, 30)->create();

        Message::create([
            'message' => 'Заполните данную анкету для получения инвестиций. Хорошо заполненная заявка, повышает шансы на поиск подходящего инвестора. После заполнения, администратор рассмотрит вашу заявки и свяжется с вами для дальнейших действий',
            'type' => Message::INTRODUCTION,
            'form_id' => $startupForms->id,
        ]);

        Message::create([
            'message' => 'Заполните данную анкету для доступа к списку стартапов. После заполнения, администратор рассмотрит вашу заявку и свяжется с вами для дальнейших действий',
            'type' => Message::INTRODUCTION,
            'form_id' => $investorsForm->id,
        ]);

        Message::create([
            'message' => 'Заявка принята. В ближайшее время с вами свяжется Администратор!',
            'type' => Message::CONFIRMATION,
            'form_id' => $startupForms->id,
        ]);

        Message::create([
            'message' => 'Заявка принята. В ближайшее время с вами свяжется Администратор!',
            'type' => Message::CONFIRMATION,
            'form_id' => $investorsForm->id,
        ]);


    }
}
