<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'token' => str_random(32),
    ];
});

$factory->define(App\Models\Field::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->company,
        'uuid' => $faker->uuid,
        'type' => $faker->randomElement($array = array('text',
            'number', 'select', 'textarea', 'file')),
        'description' => $faker->text(200),
        'status' => $faker->randomElement($array = array('enabled', 'disabled')),
        'order' => $faker->numberBetween(1, 30)
    ];
});

$factory->define(App\Models\FieldValue::class, function (Faker\Generator $faker) {
    return [
        'value' => $faker->name,
        'field_id' => 1,
    ];
});

$factory->define(App\Models\Shortcut::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->firstName,
        'user_id' => $faker->numberBetween(1, 3),
        'form_id' => $faker->numberBetween(1, 2)
    ];
});

$factory->define(App\Models\Filter::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'user_id' => $faker->numberBetween(1, 3),
        'operator' => $faker->randomElement($array = array('equals',
            'less', 'more')),
        'value' => $faker->numberBetween(1, 400)
    ];
});

$factory->define(App\Models\Event::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text(),
        'date_time_from' => $faker->dateTimeInInterval('-1 days', '+1 days',date_default_timezone_get()),
        'date_time_to' => $faker->dateTimeInInterval('now', '+1 days',date_default_timezone_get()),
        'status' => $faker->randomElement($array = array('approved', 'pending','rejected')),
        'user_id' => $faker->numberBetween(1, 3),
    ];
});


