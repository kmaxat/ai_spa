<!DOCTYPE html>
<html lang="en">
    <style>
        body { margin: 0; padding: 0; }
        table {
            border: 1px solid black;
            text-align: left;
            width: 100%;
            vertical-align: middle;
        }
        th {
            border: 1px solid black;
            text-align: center;
            vertical-align: middle;
        }
        td {
            border: 1px solid black;
            text-align: center;
            vertical-align: middle;
            word-wrap: break-word;
        }
    </style>
<body>
<table>
    <thead>
    <tr>
        @foreach($columns as $column)
            <th>{{ $column['label'] }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($rows as $row)
        <tr>
            @foreach($columns as $column)
                @if($row[$column['name']]['type'] == 'text')
                    <td style="width: 15px">{{ $row[$column['name']]['value'] }}</td>
                @elseif($row[$column['name']]['type'] == 'number')
                    <td style="width: 5px">{{ $row[$column['name']]['value'] }}</td>
                @elseif($row[$column['name']]['type'] == 'textarea')
                    <td style="width: 20px">{{ $row[$column['name']]['value'] }}</td>
                @elseif($row[$column['name']]['type'] == 'file')
                    <td style="width: 5px">{{ $row[$column['name']]['value'] }}</td>
                @else
                    <td style="width: 10px">{{ $row[$column['name']]['value'] }}</td>
                @endif
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>

</body>
</html>

