import * as types from '../mutation-types'

// initial state
const state = {
  filters: [],
  selectedFilters: []
}

// mutations
const mutations = {
  [types.SET_FILTERS] (state, filters) {
    state.filters = filters
  },
  [types.SET_SELECTED_FILTERS] (state, filters) {
    state.selectedFilters = filters
  },

  [types.REMOVE_FILTER] (state, filter) {
    const index = state.selectedFilters.indexOf(filter)
    state.selectedFilters.splice(index, 1)
  },

  [types.ADD_FILTER] (state, filter) {
    const index = state.selectedFilters.findIndex(item => item.name === filter.name)
    if (index === -1) {
      state.selectedFilters.push(filter)
    }
  }
}

// actions
const actions = {
  setFilters ({ commit }, filters) {
    commit(types.SET_FILTERS, filters)
  },
  setSelectedFilters ({ commit }, filters) {
    commit(types.SET_SELECTED_FILTERS, filters)
  },
  addFilter ({ commit }, filter) {
    commit(types.ADD_FILTER, filter)
  },
  removeFilter ({ commit }, filter) {
    commit(types.REMOVE_FILTER, filter)
  }
}

// getters
const getters = {
  filters: state => state.filters,
  selectedFilters: state => state.selectedFilters
}

export default {
  state,
  mutations,
  actions,
  getters
}
