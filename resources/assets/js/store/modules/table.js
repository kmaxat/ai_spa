import api from '../../api'
import axios from '../../utils/interceptors'
import * as types from '../mutation-types'

// initial state
const state = {
  response: null,
  filterKey: '',
  order: {},
  sortKey: ''
}

// mutations
const mutations = {
  [types.FETCH_DATA_SUCCESS] (state, { response }) {
    state.response = response
  },

  [types.UPDATE_DATA] (state, { data }) {
    state.response.data = data
  },
  [types.SET_TABLE_DATA] (state, data) {
    state.filterKey = data.filterKey
    state.order = data.order
    state.sortKey = data.sortKey
  }
}

// actions
const actions = {
  getData ({ commit }, params) {
    return new Promise(async (resolve, reject) => {
      const response = await api.getData(params)
      if (response) {
        commit(types.FETCH_DATA_SUCCESS, { response })
        resolve(response)
      } else {
        commit(types.FETCH_DATA_FAILURE)
        reject()
      }
    })
  },

  deleteData ({ commit }, params) {
    return new Promise(async (resolve, reject) => {
      const response = await api.deleteData(params)
      if (response) {
        resolve(response)
      } else {
        reject()
      }
    })
  },
  setTableData ({ commit }, data) {
    commit(types.SET_TABLE_DATA, data)
  }

}

// getters
const getters = {
  response: state => state.response,
  filteredResponse: (state, getters) => (sortkey, order, filterKey) => {
    var data = state.response.data
    if (filterKey) {
      data = data.filter(function (row) {
        return Object.keys(row).some(function (key) {
          return String(row[key].value).toLowerCase().indexOf(filterKey) > -1
        })
      })
    }
    if (sortkey) {
      data = data.slice().sort(function (a, b) {
        a = a[sortkey]
        b = b[sortkey]
        return (a === b ? 0 : a > b ? 1 : -1) * order
      })
    }

    return data
  },
  sortKey: state => state.sortKey,
  filterKey: state => state.filterKey,
  order: state => state.order
}

export default {
  state,
  mutations,
  actions,
  getters
}
