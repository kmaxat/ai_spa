import api from '../../api'
import * as types from '../mutation-types'

// initial state
const state = {
  notifications: null
}

// mutations
const mutations = {
  [types.FETCH_NOTIFICATIONS_SUCCESS] (state, { notifications }) {
    state.notifications = notifications
  }
}

// actions
const actions = {
  fetchNotifications ({ commit }) {
    return new Promise(async (resolve, reject) => {
      const notifications = await api.getData({
        url: 'api/notifications', params: {}
      })

      if (notifications) {
        commit(types.FETCH_NOTIFICATIONS_SUCCESS, { notifications })
        resolve(notifications)
      } else {
        commit(types.FETCH_NOTIFICATIONS_FAILURE)
        reject()
      }
    })
  },
  updateNotification ({ commit }, params) {
    return new Promise(async (resolve, reject) => {
      const notifications = await api.putData(params)
      if (notifications) {
        resolve(notifications)
      } else {
        reject()
      }
    })
  }
}

// getters
const getters = {
  notifications: state => state.notifications
}

export default {
  state,
  mutations,
  actions,
  getters
}
