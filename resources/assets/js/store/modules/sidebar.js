import api from '../../api'
import * as types from '../mutation-types'

// initial state
const state = {
  shortcuts: null
}

// mutations
const mutations = {
  [types.FETCH_SIDEBAR_SUCCESS] (state, { shortcuts }) {
    state.shortcuts = shortcuts
  },
  [types.UPDATE_SIDEBAR] (state, { shortcuts }) {
    state.shortcuts = shortcuts
  }
}

// actions
const actions = {
  fetchShortcuts ({ commit }) {
    return new Promise(async (resolve, reject) => {
      const shortcuts = await api.fetchShortcuts()

      if (shortcuts) {
        commit(types.FETCH_SIDEBAR_SUCCESS, { shortcuts })
        resolve(shortcuts)
      } else {
        commit(types.FETCH_SIDEBAR_FAILURE)
        reject()
      }
    })
  },
  updateSidebar ({ commit }, id) {
    const shortcuts = state.shortcuts.filter((item) => {
      return item.id !== id
    })
    commit(types.UPDATE_SIDEBAR, { shortcuts })
  }
}

// getters
const getters = {
  getShortcuts: state => state.shortcuts
}

export default {
  state,
  mutations,
  actions,
  getters
}
