import axios from '../utils/interceptors'

/**
 * Fetch the current user.
 *
 * @return {Object|undefined}
 */
async function fetchUser () {
  try {
    const { data } = await axios.get('/api/user')
    return data
  } catch (e) {}
}

/**
 * Fetch the current user.
 *
 * @return {Object|undefined}
 */
async function fetchShortcuts () {
  try {
    const { data, headers } = await axios.get('/api/shortcuts')
    data.time = headers.time
    return data
  } catch (e) {
    console.error(e)
  }
}

async function getData (params) {
  try {
    var data = setTime(await axios.get(params.url, { params: params.params }))
    return data
  } catch (e) {
    console.error(e)
  }
}

async function deleteData (params) {
  try {
    const data = await axios.delete(params.url, { params: params.params })
    return data
  } catch (e) {
    console.error(e)
  }
}

async function putData (params) {
  try {
    const data = await axios.put(params.url, params.params)
    return data
  } catch (e) {
    console.error(e)
  }
}

function setTime (response) {
  const { data, headers } = response
  data.time = headers.time
  if (data.hasOwnProperty('meta')) {
    const pagination = data.meta.pagination
    data.current_page = pagination.current_page
    data.per_page = pagination.per_page
    data.total = pagination.total
    data.last_page = pagination.total_pages
  }
  return data
}

export default {
  fetchUser,
  fetchShortcuts,
  getData,
  deleteData,
  putData
}

