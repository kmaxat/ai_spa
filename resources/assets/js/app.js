import Vue from 'vue'
import './bootstrap'
import { sync } from 'vuex-router-sync'

import store from './store'
import routes from './routes'
import App from './components/App.vue'
import navbar from './layout_elements/navbar.vue'
import sidebar from './layout_elements/sidebar/sidebar.vue'
import modal from './components/modal.vue'
import loader from './components/loader.vue'
import makeRouter from './utils/router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import locale from 'element-ui/lib/locale/lang/ru-RU'

Vue.use(ElementUI, { locale })

export const EventBus = new Vue()
Object.defineProperty(Vue.prototype, 'EventBus', { value: EventBus })

// register
Vue.component('navbar', navbar)
Vue.component('sidebar', sidebar)
Vue.component('loader', loader)
Vue.component('modal', modal)

const router = makeRouter(routes)

sync(store, router)

new Vue({
  store,
  router,
  ...App,
  el: '#app',
    metaInfo: {
        title: 'Angel Investors',
        titleTemplate: '%s | Angel Investors',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'}
        ]
    }
})
