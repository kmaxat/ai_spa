import Vue from 'vue'
import Child from './Child.vue'

Vue.component(Child.name, Child)
