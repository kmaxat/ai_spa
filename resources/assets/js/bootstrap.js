import Vue from 'vue'
import Tether from 'tether'
import Meta from 'vue-meta'
import axios from './utils/interceptors'
import Form from './utils/form'
import Router from 'vue-router'

import './components'
import './utils/interceptors'

window.Form = Form
window.axios = axios

Vue.config.productionTip = false

Vue.use(Router)
Vue.use(Meta)

window.Tether = Tether
