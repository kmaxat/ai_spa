import { authGuard, guestGuard } from './utils/router'

import login from './pages/auth/login.vue'
import confirm from './pages/auth/confirm.vue'
import register from './pages/auth/register.vue'
import passwordEmail from './pages/auth/password/email.vue'
import passwordReset from './pages/auth/password/reset.vue'
import error404 from './pages/errors/404.vue'
import home from './pages/home.vue'

import fields from './pages/fields/fields.vue'
import field from './pages/fields/field.vue'
import reports from './pages/reports/reports.vue'
import submission from './pages/submissions/submission.vue'

import forms from './pages/forms/forms.vue'

import events from './pages/events/events.vue'
import event from './pages/events/event.vue'

import users from './pages/users/users.vue'
import user from './pages/users/user.vue'

import application from './pages/applications/applications.vue'
import messages from './pages/messages/messages.vue'

export default [
    { path: '/', redirect: '/auth/login' },

  ...authGuard([
    { path: '/home', name: 'home', component: home },
    { path: '/fields', name: 'fields', component: fields },
    { path: '/field', name: 'field', component: field },
    { path: '/reports', name: 'reports', component: reports },
    { path: '/submission', name: 'submission', component: submission },
    { path: '/forms', name: 'forms', component: forms },
    { path: '/events', name: 'events', component: events },
    { path: '/event', name: 'event', component: event },
    { path: '/users', name: 'users', component: users },
    { path: '/user', name: 'user', component: user },
    { path: '/applications', name: 'application', component: application },
    { path: '/messages', name: 'message', component: messages }
  ]),

  ...guestGuard([
      { path: '/auth/login', name: 'auth.login', component: login },
      { path: '/auth/confirm', name: 'auth.confirm', component: confirm },
      { path: '/auth/register', name: 'auth.register', component: register },
      { path: '/password/reset', name: 'password_email', component: passwordEmail },
      { path: '/password/reset/:token', name: 'password_reset', component: passwordReset }
  ]),

  // { path: '*', component: error404 }
]
