<?php

use Barryvdh\Cors\HandleCors;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::get('forms', 'FormsController@index');
    Route::get('forms/{type}/fields', 'FormsController@getFields');

    Route::get('fields/{id}', 'FieldsController@show');
    Route::put('fields/{id}', 'FieldsController@update');
    Route::post('fields', 'FieldsController@store');
    Route::delete('fields', 'FieldsController@deleteBulk');

    Route::get('submissions', 'SubmissionsController@index');
    Route::get('submissions/{id}', 'SubmissionsController@show');
    Route::post('submissions', 'SubmissionsController@store');
    Route::post('submissions/file', 'SubmissionsController@postFile');
    Route::delete('submissions', 'SubmissionsController@destroy');
    Route::put('submissions/{id}', 'SubmissionsController@update');


    Route::get('files/{id}', 'FilesController@show')->name('file.url');

    Route::get('shortcuts', 'ShortcutsController@index');
    Route::get('shortcuts/{id}', 'ShortcutsController@show');
    Route::post('shortcuts', 'ShortcutsController@store');
    Route::delete('shortcuts/{id}', 'ShortcutsController@destroy');
    Route::put('shortcuts/{id}', 'ShortcutsController@update');

    Route::post('reports', 'ReportsController@export');

    Route::get('events', 'EventsController@index');
    Route::get('events/{id}', 'EventsController@show');
    Route::put('events/{id}', 'EventsController@update');
    Route::post('events', 'EventsController@store');
    Route::delete('events', 'EventsController@destroy');

    Route::get('users', 'UsersController@index');
    Route::get('users/{id}', 'UsersController@show');
    Route::put('users/{id}', 'UsersController@update');
    Route::delete('users', 'UsersController@destroy');

    Route::get('notifications', 'NotificationsController@index');
    Route::put('notifications/{id}', 'NotificationsController@update');

    Route::get('messages', 'MessagesController@index');
    Route::put('messages/{id}', 'MessagesController@update');
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::get('confirm/{token}', 'Auth\RegisterController@confirm');
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('wp_events', 'EventsController@getAll')->middleware(HandleCors::class);
    Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider');
    Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');

});
