<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Submission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Eusebiu\JavaScript\Facades\ScriptVariables;
use App\Observers\UserObserver;
use App\Observers\SubmissionObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        ScriptVariables::add(function () {
            return [
                'baseUrl' => url('/'),
                'blogUrl' => env('BLOG_URL',''),
                'data' => auth()->user(),
            ];
        });
        DB::listen(function ($query) {
//            Log::info($query->sql);
//             Log::info($query->bindings);
            // $query->time
        });

        User::observe(UserObserver::class);
        Submission::observe(SubmissionObserver::class);
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
