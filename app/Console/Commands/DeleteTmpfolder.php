<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class DeleteTmpfolder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:tmp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily delete tmp and export folder for uploaded files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     *
     * tmp folder is generated based on date of upload e.g. year/month/day/user_id. User_id is used so during tmp
     * upload other wouldn't access tmp files that are stored in tmp folder.
     * Tmp folder deletes all folder that are not created today. Script is run daily,
     */
    public function handle()
    {
        $now = Carbon::now();
        $directories = Storage::allDirectories('tmp');
        foreach ($directories as $directory) {
            $path = explode('/', $directory);
            if (count($path) > 0 && (int) $path[1] != (int)$now->year)
                $this->deleteFolder($directory);

            if (count($path) > 2 && (int) $path[2] != (int) $now->month) {
                $this->deleteFolder($directory);
            }
            if (count($path) > 3 && (int) $path[3] != (int) $now->day) {
                $this->deleteFolder($directory);
            }
        }
        Storage::deleteDirectory('exports');
    }

    private function deleteFolder($directory)
    {
        if (Storage::exists($directory)) {
            Storage::deleteDirectory($directory);
        }
    }
}

