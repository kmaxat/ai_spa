<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SubmissionUpdate extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $submission;
    public function __construct($submission)
    {
        $this->submission = $submission;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->submission->status == 'accepted') {
            $greeting = 'Заявка №' . $this->submission->id . ' принята';
        } else {
            $greeting = 'Заявка №' . $this->submission->id . ' отклонена';
        }
        return (new MailMessage)
                    ->from('noreply@angelinvestors.kz')
                    ->subject('Статус заявки обновлен на angelinvestors.kz')
                    ->greeting($greeting)
                    ->line('Причина: '.$this->submission->reason)
                    ->line('Ссылка для просмотра заявки ' . url('/') . '/submission?id=' . $this->submission->id);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
