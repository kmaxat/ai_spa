<?php

namespace App\Notifications;

use App\Models\Submission;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewSubmission extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $submission, $user;
    public function __construct(Submission $submission)
    {
        $this->submission = $submission;
        $this->user = $submission->user()->first();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->user->role == 'startup') {
            $greeting = 'Стартап ' . $this->user->email . ' заполнил форму';
        } else {
            $greeting = 'Инвестор ' . $this->user->email . ' заполнил форму';
        }
        return (new MailMessage)
            ->from('noreply@angelinvestors.kz')
            ->subject('Заполнены новые данные на Angelinvestors.kz')
            ->greeting($greeting)
            ->line('Ссылка для просмотра данных ' . url('/') . '/submission?id=' . $this->submission->id);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
