<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ConfirmAccount extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->from('noreply@angelinvestors.kz')
            ->subject('Регистрация на Angelinvestors.kz')
            ->greeting('Добро пожаловать, на Angelinvestors.kz!')
            ->line('Если вы стартапер или просто интересуетесь, то для активации аккаунта используйте эту ссылку:' . url('/').'/auth/confirm?token='.$this->user->token)
            ->line('Если вы инвестор и хотите получить доступ к базе стартапов, перейдите по этой ссылке и ответьте на несколько дополнительных вопросов: ' . url('/').'/auth/confirm?token='.$this->user->token)
        ->salutation('Cпасибо,');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
