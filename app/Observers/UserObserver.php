<?php
namespace App\Observers;

use App\Models\User;
use App\Models\Notification;
use Log;

class UserObserver
{
    public function deleted (User $user){
        $notifications = $user->notifications()->get();
        Notification::destroy($notifications->pluck('id')->toArray());
    }
}
