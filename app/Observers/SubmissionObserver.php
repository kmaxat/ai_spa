<?php

namespace App\Observers;

use App\Models\Submission;
use App\Models\SubmissionValue;
use App\Models\Notification;
use Storage;
use Log;

class SubmissionObserver
{

    public function deleting(Submission $submission)
    {
        $submissions = $submission->notifications()->get();
        Notification::destroy($submissions->pluck('id')->toArray());

        $submissionValues = SubmissionValue::where('submission_id', $submission->id)
        ->whereHas('field',function($query){
            $query->where('type','file');
        })->get();   
    foreach($submissionValues as $submissionValue){
        if(Storage::exists($submissionValue->value))
            Log::info($submissionValue->value);
            Storage::delete($submissionValue->value);
        }
    }
}
