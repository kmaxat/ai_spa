<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    const CONFIRMATION = 'confirmation';
    const INTRODUCTION = 'introduction';

    protected $fillable = [
        'message', 'form_id', 'type'
    ];

    public function form()
    {
        return $this->belongsTo(Form::class);
    }
}
