<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $fillable = [
        'name', 'type', 'status',
    ];

    //Relationships
    public function fields()
    {
        return $this->belongsToMany(Field::class, 'form_field',
            'form_id', 'field_id');
    }

    public function shortcuts()
    {
        return $this->hasMany(Shortcut::class);
    }

    public function submissions()
    {
        return $this->hasMany(Submission::class);
    }
    public function message()
    {
        return $this->hasOne(Message::class);
    }
}
