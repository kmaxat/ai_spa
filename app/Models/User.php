<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Notifications\PasswordResetNotification;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','provider', 'provider_id', 'confirmed','token'
    ];

    protected $appends = [
        'role', 'status'
    ];

    protected $hidden = [
        'password', 'remember_token','provider', 'provider_id','token'
    ];

    //Relationships
    public function roles()
    {
        return $this->belongsToMany(Role::class)->withPivot('status');
    }

    public function submissions()
    {
        return $this->hasMany(Submission::class);
    }

    public function hasRole($role)
    {
        $result = $this->roles()->where('name', $role)->first();
        if ($result) {
            return $result;
        } else {
            return '';
        }
    }

    public function notifications()
    {
        return $this->morphMany('App\Models\Notification', 'subject');
    }
    //Scopes

    public function scopeActive($query)
    {
        return $query->whereHas('roles', function ($query) {
            $query->where('status', 1);
        });
    }

    //Attributes

    public function getRoleAttribute()
    {
        $role =  $this->roles()->first();
        if ($role) {
            return $role->name;
        } else {
            return '';
        }
    }

    public function getStatusAttribute()
    {
        $status = $this->roles()->first();
        if ($status) {
            return $status->pivot->status;
        } else {
            return 0;
        }
    }

    //JWT Authenticable contract stuff
    /**
     * @return int
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }
}
