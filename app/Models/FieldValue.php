<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FieldValue extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'field_id', 'value'
    ];

    public function field()
    {
        return $this->belongsTo(Field::class);
    }
}
