<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'name', 'description','date_time_from', 'date_time_to','user_id','status'
    ];

    //Scopes
    public function scopeEnabled($query)
    {
        return $query->where('status', '=', 'enabled');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
