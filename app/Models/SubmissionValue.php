<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubmissionValue extends Model
{
    protected $fillable = [
        'value', 'submission_id', 'field_id'
    ];

    protected $appends = [
        'file_url'
    ];

    public function submission()
    {
        return $this->belongsTo(Submission::class);
    }

    public function field()
    {
        return $this->belongsTo(Field::class);
    }


    public function getFileUrlAttribute()
    {
        if ($this->field->type == 'file') {
            return route('file.url', $this->id);
        }
    }
}
