<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    protected $fillable = [
        'shortcut_id', 'field_id', 'operator', 'value'
    ];

    public function shortcut()
    {
        return $this->belongsTo(Shortcut::class);
    }

    public function field()
    {
        return $this->belongsTo(Field::class);
    }
}
