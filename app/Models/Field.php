<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $fillable = [
        'name', 'type', 'description', 'status', 'required', 'order'
    ];

    //Relationships
    public function forms()
    {
    return $this->belongsToMany(Form::class, 'form_field',
            'field_id', 'form_id');
    }

    public function values()
    {
        return $this->hasMany(FieldValue::class);
    }

    //Scopes
    public function scopeEnabled($query)
    {
        return $query->where('status', '=', 'enabled');
    }
}
