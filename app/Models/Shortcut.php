<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shortcut extends Model
{
    protected $fillable = [
        'name', 'user_id', 'form_id'
    ];

    public function filters()
    {
        return $this->hasMany(Filter::class);
    }

    public function form()
    {
        return $this->belongsTo(Form::class);
    }
}
