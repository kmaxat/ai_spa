<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Submission extends Model
{
    use Notifiable;

    protected $fillable = [
        'form_id', 'user_id', 'status'
    ];
    //Relationships
    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function values()
    {
        return $this->hasMany(SubmissionValue::class);
    }

    public function notifications()
    {
        return $this->morphMany('App\Models\Notification', 'subject');
    }

    public function routeNotificationForMail()
    {
        return $this->user()->first()->email;
    }
}
