<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Log;


class Notification extends Model
{
    use Notifiable;

    protected $fillable = [
        'text','subject_id','subject_type', 'read'
    ];

    public function scopeUnread($query)
    {
        return $query->where('read', false);
    }

    public function subject()
    {
        return $this->morphTo();
    }

    public function routeNotificationForMail()
    {
        $adminEmails = User::active()->whereHas('roles', function ($query) {
            $query->where('name', 'admin');
        })->get();
        return $adminEmails->pluck('email');
    }
}
