<?php

namespace App\Http\Controllers;

use App\Models\Field;
use App\Models\Filter;
use App\Models\Form;
use App\Models\Shortcut;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ShortcutsController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request)
    {
        $shortcuts = Shortcut::where('user_id', Auth::id())->with('form')
            ->get();
        return $shortcuts;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'fields' => 'required|array',
            'form_type' => 'required|in:investor,startup'
        ]);

        $form = Form::where('type', $request->get('form_type'))->first();
        if (!$form) {
            abort(404, 'Form not found');
        }
        $shortcut = Shortcut::create([
            'name' => $request->get('name'),
            'user_id' => Auth::id(),
            'form_id' => $form->id
        ]);
        $fieldIds = [];
        $submissionIds = [];
        foreach ($request->get('fields') as $field) {
            if (!in_array($field['operator'], ['','is_blank','equals','does_not_equal','less_than',
                'greater_than','starts_with','contains','does_not_contain']))
            {
                abort(400, 'Operator is wrong');
            }
            if(in_array($field['field_id'], ['id','status','submitted_at','email']))
                $submissionIds[] = $field;
            else {
                $fieldIds[$field['field_id']] = $field;
            }
        }

        $ids = collect($fieldIds)->pluck('field_id');
        $fields = Field::whereIn('id', $ids)->get();
        foreach ($fields as $field) {
            if (isset($fieldIds[$field->id])) {
                Filter::create([
                    'shortcut_id' => $shortcut->id,
                    'field_id' => $fieldIds[$field->id]['field_id'],
                    'operator' => $fieldIds[$field->id]['operator'],
                    'value' => $fieldIds[$field->id]['value']
                ]);

            }
        }
        foreach ($submissionIds as $submission) {
            Filter::create([
                'shortcut_id' => $shortcut->id,
                'field_id' => $submission['field_id'],
                'operator' => $submission['operator'],
                'value' => $submission['value']
            ]);
        }

    }

    public function show($id)
    {
        $shortcut = Shortcut::where('id', $id)
            ->where('user_id', Auth::id())
            ->with(['filters.field','form'])
            ->first();
        if ($shortcut) {
            return $shortcut;
        }
    }

    public function destroy(Request $request, $id)
    {
       return Shortcut::destroy($id);
    }

    public function update(Request $request)
    {

    }
}
