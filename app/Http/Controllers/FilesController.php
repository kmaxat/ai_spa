<?php

namespace App\Http\Controllers;

use App\Models\SubmissionValue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class FilesController extends Controller
{
    public function show($id){
        $submissionValue = SubmissionValue::where('id', $id)->with(['submission', 'field'])->first();

        if (!$submissionValue) {
            abort(404);
        }
        if ($submissionValue->field->type !== 'file') {
            abort(400);
        }

        if (in_array(Auth::user()->role,['admin','investor'])) {
            return Response::download(storage_path('app/'.$submissionValue->value));
        }
        if (Auth::id() === $submissionValue->submission->user_id) {
            return Response::download(storage_path('app/'.$submissionValue->value));
        } else
            abort(401);
    }
}
