<?php

namespace App\Http\Controllers;

use App\Models\Field;
use App\Models\FieldValue;
use App\Models\Form;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class FieldsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required|in:integer,text,textarea,checkbox,file,select',
            'description' => 'required',
            'status' => 'required|in:enabled,disabled',
            'required' => 'required|boolean',
            'order' => 'required',
            'options' => 'required_if:type,select|',
        ]);
        $v->sometimes('file_type', 'required_if:type', function ($input) {
            return in_array($input,array('pdf','word','powerpoint','excel','image'));
        });

        if ($v->fails()) {
            return response()->json($v->messages(),422);
        }

        $form = Form::where('type', $request->form_type )->first();
        if ($form) {
            $field = Field::make($request->all());
            $field->uuid = Uuid::uuid1();
            $field->save();
            if ($field->type == 'select') {
                foreach ($request->get('options') as $option) {
                    FieldValue::create([
                        'field_id' => $field->id,
                        'value' => $option['value']
                    ]);
                }
            }
            if ($field->type == 'file') {
                FieldValue::create([
                    'field_id' => $field->id,
                    'value' => $request->get('file_type')
                ]);
            }
            $form->fields()->attach($field);
            return $field;
        }
        abort(404, 'Form not found');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required|in:integer,text,textarea,checkbox,file,select,list',
            'description' => 'required',
            'status' => 'required|in:enabled,disabled',
            'required' => 'required|boolean',
            'order' => 'required',
            'options' => 'required_if:type,select|array'
        ]);
        $field = Field::where('id', $id)->first();
        if ($field) {
            $field->fill($request->all());
            $field->save();
            if ($field->type == 'select') {
                $this->updateSelectField($request, $field);
            }
            if ($field->type == 'file') {
                $fieldValue = FieldValue::where('field_id', $field->id)->first();
                if ($fieldValue) {
                    $fieldValue->value = $request->get('file_type');
                    $fieldValue->save();
                }
            }
            return Field::where('id', $id)->with('values')->first();
        }
        abort(404, 'Field not found');
    }

    public function deleteBulk(Request $request)
    {
        $ids = collect($request->ids)->values();
        Field::destroy(collect($ids)->toArray());
    }

    public function show($id)
    {
        $field = Field::where('id', $id)->with('values')->first();
        if ($field) {
            return $field;
        }
    }

    /**
     * @param Request $request
     * @param         $field
     */
    private function updateSelectField(Request $request, $field)
    {
        $fieldValues = FieldValue::where('field_id', $field->id)->get()->pluck('id');
        $optionsIds = collect($request->get('options'))->pluck('id');
        $diff = $fieldValues->diff($optionsIds);
        FieldValue::destroy(collect($diff)->values()->toArray());

        foreach ($request->get('options') as $option) {
            if (in_array($option['id'], $fieldValues->toArray())) {
                FieldValue::where('id', $option['id'])->update(['value' => $option['value']]);
            } else {
                FieldValue::create([
                    'field_id' => $field->id,
                    'value' => $option['value']
                ]);
            }
        }


    }
}
