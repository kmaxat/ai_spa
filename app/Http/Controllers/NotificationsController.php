<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class NotificationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        $limit = setLimit($request->input('limit'));
        $notifications = Notification::unread()->orderByDesc('created_at')->with('subject')->paginate($limit);
        return $notifications;
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'read' => 'required|boolean',
        ]);

        $notification = Notification::where('id', $id)->first();
        if ($notification) {
            $notification->read = $request->get('read');
            $notification->save();
            return $notification;
        }
        abort(404, 'Notification not found');
    }
}
