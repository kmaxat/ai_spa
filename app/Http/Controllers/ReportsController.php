<?php

namespace App\Http\Controllers;

use App\Models\Form;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends Controller
{

    public function export(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|in:pdf,xls',
            'form_type' => 'required|in:startup,investor',
            'rows' => 'required|array',
            'columns' => 'required|array'
        ]);

        $type = $request->get('form_type');
        $form = Form::where('type', $type)->first();
        if (!$form)
            abort(404, 'Not found form');

        $filename =$type.'_report_'.Carbon::now()->format('YmdHs');

        if ($request->type == 'xls') {
            $file = Excel::create($filename, function($excel) use($filename, $request) {
                $excel->setTitle($filename);
                $excel->setCreator('AngelInvestors.kz')
                    ->setCompany('AngelInvestors.kz');
                $excel->setDescription('Report generated by Angelinvestors.kz website');
                $excel->sheet('Sheet', function($sheet) use ($request) {
                    $sheet->setOrientation('landscape');
                    $columns = $request->get('columns');
                    $rows = $request->get('rows');
                    $width = $this->getWidth($sheet, $columns);
                    $sheet->setWidth($width);
                    foreach ($rows as $row) {
                        $arr = [];
                        foreach ($columns as $column) {
                            $arr[$column['name']] = $row[$column['name']]['value'];
                        }
                        $sheet->appendRow($arr);
                    }
                });
            })->store($request->get('type'), storage_path('app/exports'), true);

        } else {
            $file = Excel::create($filename, function($excel) use($filename, $request) {
                $excel->setTitle($filename);
                $excel->setCreator('AngelInvestors.kz')
                    ->setCompany('AngelInvestors.kz');

                $excel->sheet('Sheet', function($sheet) use ($request) {
                    $sheet->setOrientation('landscape');
                    $columns = $request->get('columns');
                    $rows = $request->get('rows');
                    $sheet->loadView('pdf')
                        ->with('columns', $columns)
                        ->with('rows', $rows);
                });

            })->store($request->get('type'), storage_path('app/exports'), true);
        }

        if ($request->get('type') === 'pdf') {
            $headers = [
                'Content-Type' => 'application/pdf'
            ];
        } else
            $headers = [
                'Content-Type' => 'application/vnd.ms-excel'
            ];
        $response = Response::download($file['full'], $file['file'], $headers)
            ->deleteFileAfterSend(true);;
        return $response;
    }

    private function getWidth($sheet, $columns): array
    {
        $columnNames = collect($columns)->pluck('label');
        $sheet->appendRow($columnNames->toArray());

        $width = [];
        $columnExcelNames = range('A', 'Z');
        for ($i = 0; $i < $columnNames->count(); $i++) {

            switch ($columns[$i]['type']) {
                case 'text':
                    $width[$columnExcelNames[$i]] = 20;
                    break;
                case 'textarea':
                    $width[$columnExcelNames[$i]] = 40;
                    break;
                case 'number':
                    $width[$columnExcelNames[$i]] = 10;
                    break;
                case 'file':
                    $width[$columnExcelNames[$i]] = 1;
                    break;
                default:
                    $width[$columnExcelNames[$i]] = 15;
            }
        }
        return $width;
    }
}
