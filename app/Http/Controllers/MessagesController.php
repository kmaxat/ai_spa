<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;

class MessagesController extends Controller
{
    public function ___construct()
    {
        $this->middleware('role:admin');
    }
    public function index(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|in:investor,startup',
            'message_type' => 'in:confirmation,introduction'
        ]);
        if($request->has('message_type')){
            $messages = Message::whereHas('form', function ($query) use($request) {
                $query->where('type', $request->get('type'));
            })->where('type',$request->get('message_type'))->first();
        } else {
            $messages = Message::whereHas('form', function ($query) use($request) {
                $query->where('type', $request->get('type'));
            })->get();
        }
        return $messages;
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'message' => 'required',
        ]);

        $message = Message::where('id', $id)->first();
        if ($message) {
            $message->message = $request->get('message');
            $message->save();
            return $message;
        }
        abort(404, 'Message not found');
    }
}
