<?php

namespace App\Http\Controllers;

use App\Events\FormSubmitted;
use App\Models\Field;
use App\Models\Form;
use App\Models\Submission;
use App\Models\SubmissionValue;
use App\Notifications\SubmissionUpdate;
use App\Transformers\Transformers\SubmissionsTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use InvalidArgumentException;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;


class SubmissionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin')->only([
            'update'
        ]);
    }

    public function index(Request $request)
    {
        $limit = setLimit($request->input('limit'));
        $type = $request->query('type');
        $form = Form::where('type', $type)->first();
        if (!$form)
            abort(404, 'Not found form');

        $queries = [];
        if ($request->has('fields')) {
            $decodedFields = json_decode($request->get('fields'));
            $queries = $this->processFields($decodedFields);
        }

        if ($request->user()->role === 'startup') {
            $query[] = array('user_id', '=', Auth::id());
            if ($form->type !== 'startup') {
                abort('400', 'Not authorized');
            }
        }
        //Investor can only watch his submission and startup submissions
        if ($request->user()->role === 'investor' && $form->type == 'investor') {
            $query[] = array('user_id', '=', Auth::id());
        }

        $submissions = Submission::where('form_id', $form->id)
            ->where($query ?? [])
            ->where($queries['submission'] ?? [])
            ->whereHas('user', function($query) use ($queries){
                $query->where($queries['user'] ?? []);
            })
            ->whereHas('values', function ($query) use ($queries){
                $query->where($queries['fields'] ?? []);
            })->orderByDesc('created_at')->with(['user', 'values'])->paginate($limit);

        if ($request->get('format') === 'table') {
            $paginator = new IlluminatePaginatorAdapter($submissions);
            return fractal()
                ->collection($submissions)
                ->paginateWith($paginator)
                ->transformWith(new SubmissionsTransformer())
                ->toArray();
        } else
            return $submissions;
    }

    public function show(Request $request,$id)
    {
        if ($request->user()->role === 'startup') {
            $submission = Submission::where('id', $id)
                ->where('user_id', Auth::id())
                ->with('values.field.values')->first();
        }
        else
            $submission = Submission::where('id', $id)->with('values.field.values')->first();

        if ($submission) {
            return $submission;
        }
        abort(400, 'Not found submission');
    }

    public function destroy(Request $request)
    {
        $ids = collect($request->ids)->values();
        if ($request->user()->role === 'admin')
            $submission = Submission::destroy(collect($ids)->toArray());
        else
            $submission = Submission::where('user_id', Auth::id())
                ->whereIn('id',collect($ids)->toArray())
                ->delete();
        return $submission;
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required|in:pending,rejected,accepted'
        ]);
        $submission = Submission::where('id', $id)->first();
        if ($submission) {
            $submission->status = $request->get('status');
            if($request->has('reason'))
                $submission->reason = $request->get('reason');
            $submission->save();
            $submission->notify(new SubmissionUpdate($submission));
            return $submission;
        } else
            abort(401, 'Submission not found');

    }

    public function store(Request $request)
    {
        $uuids = collect($request->all())->keys();
        $fields = Field::enabled()->whereIn('uuid', $uuids)->get();
        $arr = [];
        foreach ($fields as $field) {
            if ($field->required) {
                $arr[$field->uuid]= 'required';
            }
        }
        $this->validate($request, $arr);
        $form = Form::whereType($request->user()->role)->first();
        if (!$form) {
            abort(400, 'Form not found');
        }
        $submission = Submission::create([
            'form_id' => $form->id,
            'user_id' => Auth::id(),
            'status' => 'pending'
        ]);
        foreach ($fields as $field) {
            if ($field->type == 'file') {
                $this->processUploadedFile($request, $field, $submission);
            } else {
                SubmissionValue::create([
                    'field_id' => $field->id,
                    'submission_id' => $submission->id,
                    'value' => $request->get($field->uuid)
                ]);
            }

        }
        event(new FormSubmitted($submission, $form));

    }

    public function postFile(Request $request)
    {
        //TODO: Check permissions for newly created folders
        $uuid = $request->get('uuid');
        $now = Carbon::now();
        $filePath = '/tmp/'.$now->year.'/'.$now->month.'/'.$now->day.'/'.Auth::id();
        $filename = str_replace(' ', '_', pathinfo($request->file('file')->getClientOriginalName(), PATHINFO_FILENAME));
        $originalName = $filename;
        $extension = $request->file('file')->getClientOriginalExtension();

        $i = 1;
        while (Storage::exists($filePath . '/' . $filename.'.' . $extension)) {
            $filename = $originalName.'_'.$i;
            $i++;
        }

        Storage::put($filePath . '/' . $filename.'.'.$extension, file_get_contents($request->file) );
        return response()->json([
            'uuid' => $uuid,
            'path' => $filePath . '/' . $filename.'.'.$extension,
            'filename' => $filename.'.'.$extension,
            'size' => $request->file('file')->getClientSize()
        ]);
    }

    /**
     * @param Request $request
     * @param         $field
     * @param         $submission
     */
    private function processUploadedFile(Request $request, $field, $submission)
    {
        $tmpPath = $request->get($field->uuid);
        $pathArray = explode('/', $tmpPath);

        if (!is_array($pathArray) && count($pathArray) < 4) {
            return false;
        }

        if (Auth::id() == $pathArray[5]) {

            $filename = File::name($request->get($field->uuid));
            $originalName = $filename;
            $extension = File::extension($request->get($field->uuid));
            $dirname = str_replace("tmp", "submissions", File::dirname($request->get($field->uuid)));

            $i = 1;
            while (Storage::exists($dirname . '/' . $filename.'.' . $extension)) {
                $filename = $originalName.'_'.$i;
                $i++;
            }

            $path = $dirname . '/' . $filename.'.' . $extension;
            if (Storage::exists($request->get($field->uuid))) {
                Storage::move($request->get($field->uuid), $path);
                SubmissionValue::create([
                    'field_id' => $field->id,
                    'submission_id' => $submission->id,
                    'value' => $path
                ]);
            }
        }
    }

    private function processFields($fields)
    {
        $fieldQuery = [];
        $submissionQuery = [];
        $userQuery = [];
        foreach ($fields as $field) {

            $operation = $this->getOperation($field);
            $value = $this->getValue($field);

            if (in_array($field->field_id, ['id', 'status', 'submitted_at']))
                $submissionQuery[] = array($field->field_id, $operation, $value);
            else if($field->field_id === 'email')
                $userQuery[] = array($field->field_id, $operation, $value);
            else {
                $fieldQuery[] = array('field_id', '=', $field->field_id);
                $fieldQuery[] = array('value', $operation, $value);
            }

        }
        return [
            'fields' => $fieldQuery,
            'submission' => $submissionQuery,
            'user' => $userQuery
        ];
    }

    private function getOperation($field)
    {
        switch ($field->operator) {
            case 'is_blank':
                return '';
            case 'equals':
                return '=';
            case 'less_than':
                return '<';
            case 'greater_than':
                return '>';
            case 'does_not_equal':
                return '<>';
            case 'does_not_contain':
                return 'NOT LIKE';
            default:
                return 'LIKE';
        }
    }

    private function getValue($field)
    {
        if (empty($field->value)) {
            return '';
        }
        if ($field->type === 'text' || $field->type === 'textarea' || $field->type === 'select') {
            if ($field->operator === 'starts_with') {
                return $field->value.'%';
            } else {
                return '%'.$field->value . '%';
            }
        } else if ($field->type === 'date') {
            $date = Carbon::parse($field->value);
            return $date->toDateString();
        } else {
            return $field->value;
        }
    }
}
