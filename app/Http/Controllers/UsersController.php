<?php

namespace App\Http\Controllers;

use App\Events\UserRegistered;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin')->except([
            'update'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = setLimit($request->input('limit'));
        $users = User::paginate($limit);
        return $users;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->first();
        if ($user) {
            return $user;
        }
        abort(400, 'Not User submission');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::info($request->all());
        $this->validate($request, [
            'status' => 'boolean',
            'role' => 'required|in:startup,investor,admin'
        ]);
        $user = User::where('id', $id)->first();
        if ($user) {
            $role = Role::where('name', $request->get('role'))->first();

            //Admin has changed the role
            if ($request->user()->role === 'admin' && $request->has('status')) {
                $user->roles()->sync([ $role->id => [ 'status' => (bool) $request->get('status')]]);

            //User has signed as social and decided to change their role
            } else if($request->get('role') !== 'admin' && Auth::id() === (int) $id) {

                if ($request->get('role') === 'startup') {
                    $user->roles()->sync([ $role->id => [ 'status' => 1]]);
                } else {
                    //user is investor their status is 0
                    $user->roles()->sync($role->id);
                    event(new UserRegistered($user));
                }
            }
            return $user;
        }
        abort(404, 'User not found');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $ids = collect($request->ids)->values();
        $user = User::destroy(collect($ids)->toArray());
    }
}
