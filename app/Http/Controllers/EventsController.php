<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class EventsController extends Controller
{
    public function index(Request $request)
    {
        $limit = setLimit($request->input('limit'));
        if ($request->user()->role == 'admin') {
            $events = Event::orderByDesc('created_at')->paginate($limit);
        } else
            $events = Event::where('user_id', Auth::id())->orderByDesc('created_at')->paginate($limit);
            return $events;
    }

    public function show(Request $request, $id)
    {
        if ($request->user()->role == 'admin') {
            $event = Event::where('id', $id)->first();
        } else
            $event = Event::where('id', $id)->where('user_id',Auth::id())->first();

        if ($event) {
            return $event;
        }
        abort(400, 'Not found event');
    }

    public function destroy(Request $request)
    {
        $ids = collect($request->ids)->values();
        $events = Event::destroy(collect($ids)->toArray());
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'date_time_from' => 'required',
            'date_time_to' => 'required',
        ]);

        if ($request->user()->role == 'admin') {
            $event = Event::where('id', $id)->first();
        } else
            $event = Event::where('id', $id)->where('user_id',Auth::id())->first();

        if ($event) {
            $from = Carbon::parse($request->get('date_time_from'))->setTimezone('Asia/Almaty');
            $to = Carbon::parse($request->get('date_time_to'))->setTimezone('Asia/Almaty');
            $event->fill([
                'name' => $request->get('name'),
                'description' => $request->get('description'),
                'date_time_from' => $from->toDateTimeString(),
                'date_time_to' => $to->toDateTimeString(),
            ]);
            if ($request->has('status') && $request->user()->role == 'admin') {
                $event->status = $request->get('status');
            }
            $event->save();
            return $event;
        }
        abort(404, 'Event not found');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'date_time_from' => 'required|date',
            'date_time_to' => 'required|date',
        ]);

        $from = Carbon::parse($request->get('date_time_from'))->setTimezone('Asia/Almaty');
        $to = Carbon::parse($request->get('date_time_to'))->setTimezone('Asia/Almaty');

        $event = Event::make([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'date_time_from' => $from->toDateTimeString(),
            'date_time_to' => $to->toDateTimeString(),
            'user_id' => Auth::id()
        ]);
        if ($request->has('status') && $request->user()->role == 'admin') {
            $event->status = $request->get('status');
        }
        $event->save();
        return $event;
    }

    public function getAll(Request $request)
    {
        if ($request->has('month')) {
            $month = $request->get('month') + 1;
        } else
            $month = date('m');
        $events = Event::whereMonth('date_time_from', $month)->get();
        return $events;
    }

}
