<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRegistered;
use App\Models\Notification;
use App\Models\Role;
use App\Models\User;
use App\Notifications\ConfirmAccount;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Laravel\Socialite\Facades\Socialite;
use Tymon\JWTAuth\Facades\JWTAuth;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        return $user;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            'role' => 'required|in:investor,startup'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'token' => str_random(32)
        ]);
        $role = Role::where('name', $data['role'])->first();
        if ($role) {
            if ($role == 'startup') {
                $user->roles()->attach($role->id,['status' => 1]);
            } else
                $user->roles()->attach($role->id,['status' => 0]);
        }
        $user->notify(new ConfirmAccount($user));
        event(new UserRegistered($user));
        return $user;
    }

    public function redirectToProvider($provider)
    {
        $url =  Socialite::with($provider)->stateless()->redirect()->getTargetUrl();
        return response()->json([
            'url' => $url
        ]);
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::with($provider)->stateless()->user();
        $authUser = $this->findOrCreateUser($user, $provider);
        $token = JWTAuth::fromUser($authUser);

        return response([
            'token' => $token,
            'user' => $authUser
        ]);
    }

    public function confirm($token)
    {
        $user = User::where('token',$token)->first();
        if ($user && isset($user->token)) {
            $user->token = null;
            $user->confirmed = true;
            $user->save();
            $token = JWTAuth::fromUser($user);
            return response([
                'token' => $token,
                'user' => $user
            ]);
        }
        return response()->json(400, 'Not found user');

    }

    private function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->orWhere('email',$user->email)->first();
        if ($authUser) {
            return $authUser;
        }
        $user = User::create([
            'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id,
            'confirmed' => true,
            'token' => null
        ]);
        $user->notify(new ConfirmAccount($user));
        return $user;
    }

}
