<?php

namespace App\Http\Controllers;

use App\Models\Field;
use App\Models\Form;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FormsController extends Controller
{
    public function __construct()
    {
//        $this->middleware('role:admin');
    }

    public function index()
    {
        $forms = Form::all();
        return $forms;
    }

    public function getFields($type, Request $request)
    {
        $limit = setLimit($request->input('limit'));
        $form = Form::where('type', $type )->first();
        if ($form) {
            if($request->user()->role == 'admin')
                $fields = Field::whereHas('forms', function ($query) use ($form) {
                    $query->where('form_id', $form->id);
                })->orderBy('order', 'asc')->paginate($limit);
            else
                $fields = Field::enabled()->whereHas('forms', function ($query) use ($form) {
                    $query->where('form_id', $form->id);
                })->orderBy('order', 'asc')->with('values')->paginate($limit);
            return $fields;
        }
        return $form;
    }
}
