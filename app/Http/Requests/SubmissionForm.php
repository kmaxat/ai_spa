<?php

namespace App\Http\Requests;

use App\Models\Field;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Validation\Validator;

class SubmissionForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        $data =  new stdClass;
//        $data->{"12"} = 37;
//        $data = (array) $data;
//        var_dump( $data );

        Log::info($this->request->all());
        $ids = collect($this->request->all())->keys();
        $fields = Field::whereIn('id', $ids)->get();
        $arr = new \stdClass();
        foreach ($fields as $field) {
            if ($field->required) {
                $arr->{ $field->uuid } = 'required';
            }
        }
//        Log::info((array) $arr);
        $test = json_decode(json_encode($arr), True);
        Log::info($test);
        return $test;
    }

    protected function formatErrors(Validator $validator)
    {
        Log::info($validator->errors()->all());
        return $validator->errors()->all();
    }
}
