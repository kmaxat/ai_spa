<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 8/16/17
 * Time: 4:44 PM
 */

namespace App\Listeners;


use App\Models\Notification as AdminNotification;
use App\Models\User;
use App\Notifications\InvestorRegistered;
use App\Notifications\NewSubmission;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class NotificationEventSubscriber
{
    public function onUserRegister($event)
    {
        if ($event->user->hasRole('investor')) {
            $notification = AdminNotification::create([
                'subject_id' => $event->user->id,
                'subject_type' => 'App\Models\User',
                'text' => 'Новый инвестор'
            ]);
            $adminEmails = User::active()->whereHas('roles', function ($query) {
                $query->where('name', 'admin');
            })->get();
            Notification::send($adminEmails, new InvestorRegistered($event->user));
        }
    }

    public function onFormSubmission($event)
    {
        $notification = AdminNotification::create([
            'subject_id' => $event->submission->id,
            'subject_type' => 'App\Models\Submission',
            'text' => 'Новые данные в '.ucfirst($event->form->type)
        ]);
        $adminEmails = User::active()->whereHas('roles', function ($query) {
            $query->where('name', 'admin');
        })->get();
        Notification::send($adminEmails,new NewSubmission($event->submission));
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\UserRegistered',
            'App\Listeners\NotificationEventSubscriber@onUserRegister'
        );

        $events->listen(
            'App\Events\FormSubmitted',
            'App\Listeners\NotificationEventSubscriber@onFormSubmission'
        );
    }
}