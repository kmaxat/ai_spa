<?php

namespace App\Transformers\Transformers;

use App\Models\Submission;
use League\Fractal\TransformerAbstract;

class SubmissionsTransformer extends TransformerAbstract
{
    function __construct()
    {

    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Submission $submission)
    {
        $fields = [
            "id" => [
                'value' => $submission->id,
                'type' => 'number',
                'id' => 'id'
            ],
            "status" => [
                'value' => $submission->status,
                'type' => 'text',
                'id' => 'status'
            ],
            "date" => [
                'value' => (string) $submission->submitted_at,
                'type' => 'date',
                'id' => 'submitted_at'
            ],
            "author" => [
                'value' => $submission->user->email,
                'type' => 'text',
                'id' => 'email'
            ],
        ];
        foreach ($submission->values as $value) {
            if ($value->field->type === 'file') {
                $fields[$value->field->name] = [
                    'value' => $value->file_url,
                    'type' => $value->field->type,
                    'id' => $value->field->id
                ];
            } else
                $fields[$value->field->name] = [
                    'value' => $value->value,
                    'type' => $value->field->type,
                    'id' => $value->field->id
                ];
        }
        return $fields;
    }
}
