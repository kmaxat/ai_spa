<?php
/**
 * Created by PhpStorm.
 * User: kmaxat
 * Date: 5/31/16
 * Time: 16:55
 */

function setLimit($limit)
{
    if (is_null($limit) || $limit < 1)
        $limit = 10;
    return $limit;
}
