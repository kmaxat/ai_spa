sudo chgrp -R www-data storage bootstrap/cache

sudo chmod -R ug+rwx storage bootstrap/cache

sudo adduser $USER www-data

sudo chown $USER:www-data -R /var/www

sudo chmod u=rwX,g=srX,o=rX -R /var/www
